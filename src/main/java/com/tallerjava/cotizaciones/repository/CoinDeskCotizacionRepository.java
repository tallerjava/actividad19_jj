package com.tallerjava.cotizaciones.repository;

import com.tallerjava.cotizaciones.modelo.Cotizacion;
import java.util.Date;
import jodd.http.HttpRequest;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class CoinDeskCotizacionRepository extends CotizacionRepository {

    private String url = "https://api.coindesk.com/v1/bpi/currentprice.json";
    private String nombre = "CoinDesk";

    public String getUrl(String url) {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String getNombre() {
        return nombre;
    }

    @Override
    public Cotizacion obtenerCotizacion() {
        try {
            Cotizacion cotizacion;
            jodd.http.HttpResponse response = HttpRequest.get(url).send();
            JSONObject namecampo = new JSONObject(response.body());
            String moneda = namecampo.getJSONObject("bpi").getJSONObject("USD").getString("code");
            double cotizacionBitcoin = namecampo.getJSONObject("bpi").getJSONObject("USD").getDouble("rate_float");
            Date fecha = new Date();
            cotizacion = new Cotizacion(nombre, fecha, moneda, cotizacionBitcoin);
            return cotizacion;
        } catch (RuntimeException ex) {
            throw new CotizacionNoObtenidaException(ex.getMessage(), ex);
        }
    }
}
