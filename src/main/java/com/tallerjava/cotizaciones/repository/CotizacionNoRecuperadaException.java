package com.tallerjava.cotizaciones.repository;

public class CotizacionNoRecuperadaException extends RuntimeException {

    public CotizacionNoRecuperadaException() {
        super();
    }

    public CotizacionNoRecuperadaException(String msg, Throwable causa) {
        super(msg, causa);
    }

    public CotizacionNoRecuperadaException(String msg) {
        super(msg);
    }

    public CotizacionNoRecuperadaException(Throwable causa) {
        super(causa);
    }
}
