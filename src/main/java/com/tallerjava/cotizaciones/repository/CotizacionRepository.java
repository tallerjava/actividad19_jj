package com.tallerjava.cotizaciones.repository;

import com.tallerjava.cotizaciones.modelo.Cotizacion;

public abstract class CotizacionRepository {

    public abstract Cotizacion obtenerCotizacion();

    public abstract String getNombre();
}
