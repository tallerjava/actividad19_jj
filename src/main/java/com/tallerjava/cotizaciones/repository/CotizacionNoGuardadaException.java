package com.tallerjava.cotizaciones.repository;

public class CotizacionNoGuardadaException extends RuntimeException {

    public CotizacionNoGuardadaException() {
        super();
    }

    public CotizacionNoGuardadaException(String msg, Throwable causa) {
        super(msg, causa);
    }

    public CotizacionNoGuardadaException(String msg) {
        super(msg);
    }

    public CotizacionNoGuardadaException(Throwable causa) {
        super(causa);
    }
}
