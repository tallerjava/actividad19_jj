package com.tallerjava.cotizaciones.repository;

import com.tallerjava.cotizaciones.modelo.Cotizacion;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.springframework.stereotype.Component;

@Component
public class FinanSurCotizacionRepository extends CotizacionRepository {

    private String nombre = "Finan Sur";
    private String usuario = "Taller";
    private String password = "1234";

    @Override
    public String getNombre() {
        return nombre;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    @Override
    public Cotizacion obtenerCotizacion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            try (Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/finansur", usuario, password); Statement statement = conexion.createStatement()) {
                ResultSet result = statement.executeQuery(
                        "select proveedor, fecha_cotizacion, moneda, precio "
                        + "from cotizacion "
                        + "where proveedor = '" + nombre + "' "
                        + "order by fecha_cotizacion desc;");
                if (result.next()) {
                    return new Cotizacion(nombre, result.getTimestamp("fecha_cotizacion"), result.getString("moneda"), result.getDouble("precio"));
                } else {
                    throw new CotizacionNoObtenidaException("No hay cotizaciones");
                }
            }
        } catch (ClassNotFoundException | SQLException ex) {
            throw new CotizacionNoObtenidaException(ex.getMessage(), ex);
        }
    }
}
