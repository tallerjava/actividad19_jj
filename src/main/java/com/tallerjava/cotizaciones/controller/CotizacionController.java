package com.tallerjava.cotizaciones.controller;

import com.tallerjava.cotizaciones.modelo.Cotizacion;
import com.tallerjava.cotizaciones.repository.BinanceCotizacionRepository;
import com.tallerjava.cotizaciones.repository.CoinDeskCotizacionRepository;
import com.tallerjava.cotizaciones.repository.CotizacionRepository;
import com.tallerjava.cotizaciones.repository.CryptoCotizacionRepository;
import com.tallerjava.cotizaciones.repository.FinanSurCotizacionRepository;
import com.tallerjava.cotizaciones.repository.SomosPNTCotizacionRepository;
import com.tallerjava.cotizaciones.service.CotizacionService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CotizacionController {

    @Autowired
    private CotizacionService service;

    @GetMapping(value = "/index")
    public String index() {
        return "index";
    }

    @GetMapping(value = "/mostrarcotizacion")
    public String mostrarcotizacion(ModelMap modelo) {
        List<Cotizacion> cotizaciones = service.obtenerCotizaciones();
        modelo.addAttribute("listadoCotizacion", cotizaciones);
        return "mostrarcotizacion";
    }
}
