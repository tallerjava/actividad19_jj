package com.tallerjava.cotizaciones.service;

import com.tallerjava.cotizaciones.modelo.Cotizacion;
import com.tallerjava.cotizaciones.repository.BackupCotizacion;
import com.tallerjava.cotizaciones.repository.CotizacionNoGuardadaException;
import com.tallerjava.cotizaciones.repository.CotizacionNoObtenidaException;
import com.tallerjava.cotizaciones.repository.CotizacionNoRecuperadaException;
import com.tallerjava.cotizaciones.repository.CotizacionRepository;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CotizacionService {

    @Autowired
    private List<CotizacionRepository> repositorios;
    private final BackupCotizacion backupCotizacion = new BackupCotizacion();
    private final static Logger LOGGER = Logger.getLogger(CotizacionService.class.getName());

    public CotizacionService() {
    }

    public void setRepositorios(List<CotizacionRepository> repositorios) {
        this.repositorios = repositorios;
    }

    public List<Cotizacion> obtenerCotizaciones() {

        List<Cotizacion> cotizaciones = new ArrayList();
        Cotizacion cotizacion;
        FileHandler fileHandler;
        try {
            fileHandler = new FileHandler("serviceCotizacion.txt");
            SimpleFormatter simpeFormatter = new SimpleFormatter();
            fileHandler.setFormatter(simpeFormatter);
            LOGGER.addHandler(fileHandler);
        } catch (IOException ex) {
            Logger.getLogger(CotizacionService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(CotizacionService.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (CotizacionRepository repositorio : repositorios) {
            try {
                cotizacion = repositorio.obtenerCotizacion();
                cotizaciones.add(cotizacion);
                if (!"Finan Sur".equals(repositorio.getNombre())) {
                    backupCotizacion.guardarCotizacion(cotizacion);
                }
            } catch (CotizacionNoGuardadaException cotizacionNoOGuardadaException) {
                LOGGER.log(Level.SEVERE, null, cotizacionNoOGuardadaException);
            } catch (CotizacionNoObtenidaException e) {
                try {
                    cotizaciones.add(backupCotizacion.recuperarCotizacion(repositorio.getNombre()));
                } catch (CotizacionNoRecuperadaException cotizacionNoRecuperadaException) {
                    cotizaciones.add(new Cotizacion(repositorio.getNombre(), null, null, 0.0f));
                }
            }
        }
        return cotizaciones;
    }

}
