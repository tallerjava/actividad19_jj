package com.tallerjava.repository;

import com.tallerjava.cotizaciones.modelo.Cotizacion;
import com.tallerjava.cotizaciones.repository.BackupCotizacion;
import com.tallerjava.cotizaciones.repository.CoinDeskCotizacionRepository;
import com.tallerjava.cotizaciones.repository.CotizacionNoGuardadaException;
import com.tallerjava.cotizaciones.repository.CotizacionNoRecuperadaException;
import com.tallerjava.cotizaciones.repository.CotizacionRepository;
import java.util.Date;
import static org.junit.Assert.*;
import org.junit.Test;

public class BackupCotizacionTest {

    @Test
    public void guardarCotizacion_backupOk_seGuardaLaCotizacion() {
        CotizacionRepository repositorio = new CoinDeskCotizacionRepository();
        Cotizacion cotizacion = new Cotizacion(repositorio.getNombre(),new Date(),"USD",7993.66f);
        BackupCotizacion backup = new BackupCotizacion();
        backup.guardarCotizacion(cotizacion);
    }

    @Test(expected = CotizacionNoGuardadaException.class)
    public void guardarCotizacion_backupFalla_cotizacionNoGuardadaException() {
        CotizacionRepository repositorio = new CoinDeskCotizacionRepository();
        Cotizacion cotizacion = new Cotizacion(repositorio.getNombre(), new Date(), "USD", 7993.66f);
        BackupCotizacion backup = new BackupCotizacion();
        backup.setUsuario("User");
        backup.guardarCotizacion(cotizacion);
    }

    @Test
    public void recuperarCotizacion_proveedorCoindeskBackupOk_seRecuperaLaCotizacion() {
        CotizacionRepository repositorio = new CoinDeskCotizacionRepository();
        Cotizacion resultadoEsperado = new Cotizacion(repositorio.getNombre(), new Date(), "USD", 7993.66f);
        BackupCotizacion backup = new BackupCotizacion();
        backup.guardarCotizacion(resultadoEsperado);
        Cotizacion resultadoObtenido = backup.recuperarCotizacion(repositorio.getNombre());
        assertNotNull(resultadoObtenido);
    }

    /*
    @Test(expected = CotizacionNoRecuperadaException.class)
    public void recuperarCotizacion_proveedorCoindeskSinCotizacionEnElBackup_cotizacionNoRecuperadaException() {

    }
    */
    
    @Test(expected = CotizacionNoRecuperadaException.class)
    public void recuperarCotizacion_proveedorCoindeskBackupFalla_cotizacionNoRecuperadaException() {
        CotizacionRepository repositorio = new CoinDeskCotizacionRepository();
        Cotizacion cotizacion = new Cotizacion(repositorio.getNombre(), new Date(), "USD", 7993.66f);
        BackupCotizacion backup = new BackupCotizacion();
        backup.setUsuario("User");
        backup.recuperarCotizacion(cotizacion.getProveedor());
    }

}
