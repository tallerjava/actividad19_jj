package com.tallerjava.repository;

import com.tallerjava.cotizaciones.modelo.Cotizacion;
import com.tallerjava.cotizaciones.repository.CoinDeskCotizacionRepository;
import com.tallerjava.cotizaciones.repository.CotizacionNoObtenidaException;
import org.junit.Test;
import static org.junit.Assert.*;

public class CoinDeskCotizacionRepositoryTest {
    
    @Test
    public void obtenerCotizacion_servicioResponde_objetoCotizacionNotNull() {
        CoinDeskCotizacionRepository instance = new CoinDeskCotizacionRepository();
        Cotizacion resultadoObtenido = instance.obtenerCotizacion();
        assertNotNull(resultadoObtenido);
    }

    @Test(expected = CotizacionNoObtenidaException.class)
    public void obtenerCotizacion_urlIncorrecta_httpException() {
        CoinDeskCotizacionRepository instance = new CoinDeskCotizacionRepository();
        instance.setUrl("https://api.coindes.com/v1/bpi/currentprice.json");
        Cotizacion resultadoObtenido = instance.obtenerCotizacion();
    }    
}
