package com.tallerjava.service;


import com.tallerjava.cotizaciones.modelo.Cotizacion;
import com.tallerjava.cotizaciones.repository.BinanceCotizacionRepository;
import com.tallerjava.cotizaciones.repository.CoinDeskCotizacionRepository;
import com.tallerjava.cotizaciones.repository.CotizacionRepository;
import com.tallerjava.cotizaciones.repository.CryptoCotizacionRepository;
import com.tallerjava.cotizaciones.repository.FinanSurCotizacionRepository;
import com.tallerjava.cotizaciones.repository.SomosPNTCotizacionRepository;
import com.tallerjava.cotizaciones.service.CotizacionService;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class CotizacionServiceTest {
    /*
   @Test
    public void obtenerCotizacion_servicioResponde_objetoCotizacionNotNull() {
        List<CotizacionRepository> repositorios = new ArrayList();
        repositorios.add(new CoinDeskCotizacionRepository());
        repositorios.add(new BinanceCotizacionRepository());
        repositorios.add(new SomosPNTCotizacionRepository());
        repositorios.add(new CryptoCotizacionRepository());
        repositorios.add(new FinanSurCotizacionRepository());
        CotizacionService instance = new CotizacionService(repositorios);
        List<Cotizacion> resultadoObtenido = instance.obtenerCotizaciones();
        assertTrue(resultadoObtenido.get(0).getPrecio() > 0);
        assertTrue(resultadoObtenido.get(1).getPrecio() > 0);
        assertTrue(resultadoObtenido.get(2).getPrecio() > 0);
        assertTrue(resultadoObtenido.get(3).getPrecio() > 0);
        assertTrue(resultadoObtenido.get(4).getPrecio() > 0);
    }

    @Test
    public void obtenerCotizacion_coinDeskFalla_seRecuperaLaCotizacionDelBackup() {
        List<CotizacionRepository> repositorios = new ArrayList();
        CoinDeskCotizacionRepository coinDesk = new CoinDeskCotizacionRepository();
        coinDesk.setUrl("https://api.coindes.com/v1/bpi/currentprice.json");
        repositorios.add(coinDesk);
        CotizacionService instance = new CotizacionService(repositorios);
        List<Cotizacion> resultadoObtenido = instance.obtenerCotizaciones();
        assertTrue(resultadoObtenido.get(0).getPrecio() > 0);
    }
     */
}
